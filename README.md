# Axiomatization of the Well-Ordering Principle in Why3 #

This project shows a constructive axiomatization of [the well-ordering principle][1], based on [Carsten Hornung's BSc thesis][2].

The goal was to exercise [Why3][3] and distill the minimum axioms required for a constructive proof of this result.

Also included is a model of this axiomatization: natural numbers have the well-ordering principle. As Why3 does not include naturals in its library, I implemented a quick an encoding of natural numbers that uses the integers of the standard library.

*Requirements*: Why3, [Coq][4] and [Z3][5].

To execute the proof script, run:

    make

**LICENSE:** Public Domain

  [1]: http://en.wikipedia.org/wiki/Well-ordering_principle
  [2]: http://www.ps.uni-saarland.de/~hornung/bachelor.php
  [3]: http://why3.lri.fr/
  [4]: http://coq.inria.fr/
  [5]: z3.codeplex.com/
  