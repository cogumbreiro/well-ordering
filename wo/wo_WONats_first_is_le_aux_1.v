(* This file is generated by Why3's Coq driver *)
(* Beware! Only edit allowed sections below    *)
Require Import BuiltIn.
Require BuiltIn.
Require bool.Bool.
Require int.Int.

(* Why3 assumption *)
Inductive nat :=
  | Zero : nat
  | Succ : nat -> nat.
Axiom nat_WhyType : WhyType nat.
Existing Instance nat_WhyType.

(* Why3 assumption *)
Fixpoint nat2int (n:nat) {struct n}: Z :=
  match n with
  | Zero => 0%Z
  | (Succ n1) => (1%Z + (nat2int n1))%Z
  end.

(* Why3 assumption *)
Definition lt (x:nat) (y:nat): Prop := ((nat2int x) < (nat2int y))%Z.

(* Why3 assumption *)
Inductive le : nat -> nat -> Prop :=
  | le_lt : forall (x:nat) (y:nat), (lt x y) -> (le x y)
  | le_eq : forall (x:nat), (le x x).

Axiom nat2int_succ_gt_zero : forall (x:nat), (0%Z < (nat2int (Succ x)))%Z.

Axiom nat2int_eq : forall (x:nat) (y:nat), ((nat2int x) = (nat2int y)) ->
  (x = y).

Axiom le_le_succ : forall (x:nat) (y:nat), (le x y) -> (le x (Succ y)).

Parameter p: nat -> Prop.

Parameter first: nat -> nat.

Axiom first_def : forall (n:nat),
  match n with
  | Zero => ((first n) = Zero)
  | (Succ x) => ((p (first x)) -> ((first n) = (first x))) /\ ((~ (p
      (first x))) -> ((first n) = (Succ x)))
  end.

Axiom first_in_p_aux : forall (x:nat), (p x) -> (p (first x)).

Axiom first_is_le_aux1 : forall (x:nat), (p x) \/ ~ (p x).

Axiom first_is_le_aux2 : forall (x:nat) (y:nat), (le x y) -> (le x (Succ y)).

(* Why3 goal *)
Theorem first_is_le_aux : forall (x:nat), (le (first x) x).
(* Why3 intros x. *)
intros x.
induction x.
 - assert (H := first_def Zero). compute in H.
   rewrite H.
   apply le_eq.
 - assert (H := first_def (Succ x)). compute in H.
   elim H. clear H.
   intros H1 H2.
   assert (H:= first_is_le_aux1 (first x)).
   elim H.
   + clear H. intros H.
     apply H1 in H.
     rewrite H.
     apply first_is_le_aux2.
     assumption.
   + clear H. intros H.
     apply H2 in H.
     rewrite H.
     apply le_eq.
Qed.

