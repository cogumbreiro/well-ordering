all:
	why3replayer --force wo.why

clean:
	./rm-old-proofs *.why

doc:
	why3doc wo.why --title "Showing the well-ordering principle in Why3." -o html
